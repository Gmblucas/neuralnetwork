//
// Created by Lucas Gambini on 17/12/2019.
//

#include "TrainingNetwork.hpp"

TrainingNetwork::TrainingNetwork(std::vector<int> const &shape, double learningRate) {
    this->_learningRate = learningRate;

    for (unsigned i = 0; i < shape.size()-1; i++) {
        this->_weights.emplace_back(arma::mat(shape[i], shape[i+1], arma::fill::randn));
        this->_bias.emplace_back(arma::mat(1, shape[i], arma::fill::randn));
        this->_layers.emplace_back(arma::mat(1, shape[i], arma::fill::zeros));
    }
    this->_layers.emplace_back(arma::mat(1, shape[shape.size()-1], arma::fill::zeros));
    this->_bias.emplace_back(arma::mat(1, shape[shape.size()-1], arma::fill::randn));
}

void TrainingNetwork::train(std::string const &trainingFile, unsigned iteration) {
    std::ifstream file(trainingFile);
    std::string line;
    size_t useless = 0;
    size_t pos = 0;

    getline(file, line); //skipping headers
    for (unsigned i = 0; i < iteration ; i++) {
        getline(file, line);
        std::string token;
        int j = 0;
        while ((pos = line.find(',')) != std::string::npos) {
            token = line.substr(0, pos);
            this->_layers.front()(0, std::stod(token.c_str(), &useless)) = 1;
            //this->_layers.front()(j++, 0) = std::stod(token.c_str(), &useless);
            line.erase(0, pos + 1);
        }
        unsigned int n = std::atoi(line.c_str());
        this->_layers.back().zeros();
        this->_layers.back()(0, n) = 1;
        arma::mat expectedOutput = this->_layers.back();
        this->_feedForward();
        this->_computeError(expectedOutput, i);
        this->_backProp(expectedOutput);

        std::cout << "Expected:" << std::endl;
        expectedOutput.print();
        std::cout << "Predicted:" << std::endl;
        this->_layers.back().print();
        std::cout << std::endl << std::endl;

        if (file.eof()) {
            std::clog << "restarting from the beginning of the file" << std::endl;
            file.clear();
            file.seekg(0, std::ios::beg);
            getline(file, line); //skipping headers
        }
    }
}

void TrainingNetwork::test(std::string const &testFile) {

}

void TrainingNetwork::_computeError(arma::mat const &expectedOutput, unsigned iteration) {
    double error = 0;
    for (unsigned i = 0; i < expectedOutput.size(); i++) {
        error += pow(expectedOutput[i] - this->_layers.back()[i], 2);
    }
    error /= expectedOutput.size();
    error = sqrt(error);
    this->_errors.emplace_back(error);
}

void TrainingNetwork::_feedForward() {
    arma::mat tmp;
    for (unsigned i = 1; i < this->_layers.size(); i++) {
        tmp = (this->_layers[i-1] * this->_weights[i-1]) + this->_bias[i];
        tmp.transform([](double v) { return tanh(v); });
        this->_layers[i] = tmp;
    }
}

void TrainingNetwork::_backProp(arma::mat const &expectedOutput) {
    std::vector<arma::mat> gradients(this->_layers.size());

    arma::mat tmp = expectedOutput - this->_layers.back();
    tmp.transform([](double v) {return v * (1 - tanh(v)*tanh(v));});
    gradients.back() = tmp;

    tmp = this->_weights[0].t() * gradients.back().t();
    arma::mat tanh_out = this->_layers[1];
    tanh_out.transform([](double v) { return tanh(v); });
    tanh_out *= tanh_out.t();
    gradients[1] = tmp * (1.0 - tanh_out);

    tmp = gradients.back().t() * this->_layers[1] * this->_learningRate;
    this->_weights[1] += tmp.t();

    tmp = gradients[1] * this->_layers[0] * this->_learningRate;
    this->_weights[0] += tmp.t();

}

void TrainingNetwork::save() {
    for (unsigned i = 0; i < this->_weights.size(); i++) {
        this->_weights[i].save("../saves/matrix" + std::to_string(i+1) + ".dat", arma::raw_ascii);
    }
}

void TrainingNetwork::plotErrors() {
    plt::backend("webagg");
    plt::plot(this->_errors);
    plt::xlabel("Iterations");
    plt::ylabel("Error rate");
    plt::show();
}


