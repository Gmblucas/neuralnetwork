//
// Created by Lucas Gambini on 17/12/2019.
//

#include <iostream>
#include <TrainingNetwork.hpp>

int main(int argc, char *argv[]) {
    std::vector<int> shape{2, 4, 2};
    double learningRate = 0.05;
    TrainingNetwork network(shape, learningRate);
    network.train(std::string("../samples/xor.csv"), 100);
    //Network network(std::string("../saves/"));
    network.save();
    network.plotErrors();
    return 0;
}