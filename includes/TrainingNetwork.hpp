//
// Created by Lucas Gambini on 17/12/2019.
//

#ifndef NEURALNETWORK_TRAININGNETWORK_HPP
#define NEURALNETWORK_TRAININGNETWORK_HPP

#include "matplotlibcpp.h"
#include "ANetwork.hpp"

namespace plt = matplotlibcpp;

class TrainingNetwork : public ANetwork {

private:
    std::vector<double> _errors;
    std::vector<arma::mat> _layers;
    double _learningRate;

    void _feedForward();
    void _computeError(arma::mat const &expectedOutput, unsigned iteration);
    void _backProp(arma::mat const &expectedOutput);

public:
    TrainingNetwork() = delete;
    explicit TrainingNetwork(std::vector<int> const &shape, double learningRate);

    void train(std::string const &trainingFile, unsigned iteration);
    void test(std::string const &testFile);
    void save();
    void plotErrors();

};

#endif //NEURALNETWORK_TRAININGNETWORK_HPP
