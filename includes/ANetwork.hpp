//
// Created by Lucas Gambini on 17/12/2019.
//

#ifndef NEURALNETWORK_ANETWORK_HPP
#define NEURALNETWORK_ANETWORK_HPP

#include <armadillo>

class ANetwork {

protected:
    std::vector<arma::mat> _weights;
    std::vector<arma::mat> _bias;

public:
    ANetwork();
    //explicit TrainingNetwork(std::string const &directory);

};

#endif //NEURALNETWORK_ANETWORK_HPP


//Network tout court = predict(), construit from directory like -->
//explicit TrainingNetwork(std::string const &directory);
